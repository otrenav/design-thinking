
| [Website](http://links.otrenav.com/website) | [Twitter](http://links.otrenav.com/twitter) | [LinkedIn](http://links.otrenav.com/linkedin)  | [GitHub](http://links.otrenav.com/github) | [GitLab](http://links.otrenav.com/gitlab) | [CodeMentor](http://links.otrenav.com/codementor) |

---

# Design Thinking Road Map

- Omar Trejo
- March, 2017

Road map for 5 days of activities using Design Thinking process.

TODOs:

- [ ] Single files per day
- [ ] Detailed content

---

> "The best ideas are common property."
>
> —Seneca
